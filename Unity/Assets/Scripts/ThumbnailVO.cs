﻿
public class ThumbnailVO
{
    // Thumbnail ID
	public string id;

    // Relevant variables for sizing, spacing, and screen bounds.
    public float _yCellSize, _ySpacing, _screenBounds, _offScreenBuffer;

	public ThumbnailVO() {}
}