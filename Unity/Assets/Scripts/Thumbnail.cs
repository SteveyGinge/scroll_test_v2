﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Thumbnail:MonoBehaviour
{
	private ThumbnailVO _thumbnailVO; 
	
	public ThumbnailVO thumbnailVO {
		get {
			return _thumbnailVO;
		}
		set {
			_thumbnailVO = value;
            // Call to set the image and text components.
            SetThumbnailDetails(value.id);
		}
	}

    // Values required for accurate spacing and when to update.
    public int _thumbnailPoolingCount, _totalThumbnailCount, _constraintCount;

    // Values used to reduce calculations and parsing on the fly.
    private int _rowSpacing, _parsedId;

    // Values used to reduce calculations for spacing and movement of thumbnails.
    private float _yTotalSpacing, _yTotalMovement, _extraYTopBuffer = 0.5f;

    // References to prefab components to minimise GetComponent calls.
    [SerializeField]
    private Image _imageSprite;
    [SerializeField]
    private Text _text;

    /// <summary>
    /// Populate refences to other components if they are null.
    /// </summary>
    private void Awake()
    {
        if(_imageSprite == null)
        {
            _imageSprite = GetComponent<Image>();
        }

        if(_text == null)
        {
            _text = GetComponentInChildren<Text>();
        }

    }

    /// <summary>
    /// Dynamically calculate the total spacing and row spacing required to move the thumbnail
    /// </summary>
    private void Start()
    {
        // Work this out now so constant calculations are minimised.
        _yTotalSpacing = (_thumbnailVO._yCellSize + _thumbnailVO._ySpacing);
        _rowSpacing = (_thumbnailPoolingCount / _constraintCount);
        _parsedId = int.Parse(_thumbnailVO.id);
    }

    /// <summary>
    /// Set the thumbnail details
    /// </summary>
    /// <param name="id">Thumbnail id</param>
    private void SetThumbnailDetails(string id)
    {
        _imageSprite.sprite = SpriteManager.instance.GetSprite(_thumbnailVO.id);
        _text.text = _thumbnailVO.id.ToString();
    }

    /// <summary>
    /// Button OnClick call
    /// </summary>
	public void OnClick() {
		Debug.Log("Thumbnail clicked: "+_thumbnailVO.id);
	}

    /// <summary>
    /// Using LateUpdate to check after the screen has been updated.
    /// </summary>
    protected void LateUpdate()
    {
        // If thumbnail has passed the upper bounds.
        if (transform.position.y > _thumbnailVO._screenBounds + _thumbnailVO._offScreenBuffer + _extraYTopBuffer)
        {
            // If this thumbnail plus the pooling count is still within the total count, move it
            if (_parsedId + _thumbnailPoolingCount < _totalThumbnailCount)
            {
                MoveThumbnailDown();
            }
        }
        // If thumbnail has passed the lower bounds.
        else if (transform.position.y < -_thumbnailVO._screenBounds - _thumbnailVO._offScreenBuffer)
        {
            // If this thumbnail plus the pooling count is still within the total count, move it
            if (_parsedId - _thumbnailPoolingCount >= 0)
            {
                MoveThumbnailUp();
            }
        }
    }

    /// <summary>
    /// Move the thumbnail to the bottom of the pooled thumbnails.
    /// </summary>
    private void MoveThumbnailDown()
    {
        // Work out new thumbnail ID from current ID plus pooling count.
        _parsedId = int.Parse(_thumbnailVO.id) + _thumbnailPoolingCount;
        _thumbnailVO.id = _parsedId.ToString();
        SetThumbnailDetails(_thumbnailVO.id);

        // Calculate movement based on current position, total spacing, and row spacing.
        _yTotalMovement = transform.localPosition.y - (_yTotalSpacing * _rowSpacing);
        transform.localPosition = new Vector3(transform.localPosition.x, _yTotalMovement, 0f);
    }   

    /// <summary>
    /// Move the thumbnail to the top of the pooled thumbnails.
    /// </summary>
    private void MoveThumbnailUp()
    {
        // Work out new thumbnail ID from current ID minus pooling count.
        _parsedId = int.Parse(_thumbnailVO.id) - _thumbnailPoolingCount;
        _thumbnailVO.id = _parsedId.ToString();
        SetThumbnailDetails(_thumbnailVO.id);

        // Calculate movement based on current position, total spacing, and row spacing.
        _yTotalMovement = transform.localPosition.y + (_yTotalSpacing * _rowSpacing);
        transform.localPosition = new Vector3(transform.localPosition.x, _yTotalMovement, 0f);
    }
}