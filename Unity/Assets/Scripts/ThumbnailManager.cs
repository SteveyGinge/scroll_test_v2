﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ThumbnailManager:MonoBehaviour {

    public Transform container;
	public GameObject prefab;

	private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO>();

    // Total limit and object pooling limit variables
    [SerializeField]
    protected int _maxPrefabLimit = 1000, _prefabPooling = 100;

    // Replacement for optimizing the Grid Layout Group to permit object pooling
    // Total X and Y movement and buffer variables.
    private int _totalXMovement, _totalYMovement, _xBuffer, _yBuffer;

    // X and Y Positions used to replicate the grid layout.
    private float _xPos, _yPos;
    // Vertical bound and buffer to be used when objects are off screen.
    private float _verticalBound, _offScreenBuffer;
    
    // Object reference so new ones are not always created during for loops.
    private Thumbnail _thumbnailInstantiated;

    // Cell size and spacing values.
    [SerializeField]
    protected Vector2 _cellSize, _spacing;

    // How many items to fit per row
    [SerializeField]
    protected int _constraintCount = 5;

    // Rect Transform variable to set the size of the content based on "_maxPrefabLimit"
    [SerializeField]
    protected RectTransform _contentRectTransform;

    /// <summary>
    /// Populate references and calculate variables at the start.
    /// This is to prevent too many on the fly calculations.
    /// </summary>
    private void Awake()
    {
        _verticalBound = Camera.main.orthographicSize;
        // Setting the offScreenBuffer to be a third of the screen size
        _offScreenBuffer = (_prefabPooling / _constraintCount) / 3;

        // halfing the cell size to get initial x and y buffers (so the thumbnail isn't half off screen to start)
        _xBuffer = (int)_cellSize.x / 2;
        _yBuffer = (int)_cellSize.y / 2;

        // Total movement between each thumbnail is the cell size plus the spacing for each axis
        _totalXMovement = Mathf.CeilToInt(_cellSize.x + _spacing.x);
        _totalYMovement = Mathf.CeilToInt(_cellSize.y + _spacing.y);

        // Calculate and set the size of the Content based on the max prefab limit and the cell size.
        // Once this is set this will allow for the rows of thumbnails to be moved without resizing the Content as it goes.
        _contentRectTransform.sizeDelta = new Vector2(((_cellSize.x + _spacing.x) * _constraintCount - _spacing.x), ((_cellSize.y + _spacing.y) * (_maxPrefabLimit / _constraintCount)));
    }

    /// <summary>
    /// Create ThumbnailVO list and the prefabs.
    /// </summary>
    private void Start ()
    {
		createThumbnailVOList();
		createThumbnailPrefabs();
    }
	
    /// <summary>
    /// Create ThumbnailVO list and pass over relevant variables.
    /// </summary>
	private void createThumbnailVOList()
    {
		ThumbnailVO thumbnailVO;
        // Only create prefabs up to the pooling amount
		for (int i=0; i< _prefabPooling; i++)
        {
			thumbnailVO = new ThumbnailVO();
			thumbnailVO.id = i.ToString();
            thumbnailVO._yCellSize = _cellSize.y;
            thumbnailVO._ySpacing = _spacing.y;
            thumbnailVO._screenBounds = _verticalBound;
            thumbnailVO._offScreenBuffer = _offScreenBuffer;
            _thumbnailVOList.Add(thumbnailVO);
        }
	}

    /// <summary>
    /// Create Thumbnail prefabs and pass through relevant variables.
    /// </summary>
	private void createThumbnailPrefabs() {
		GameObject gameObj;
		for (int i = 0; i < _thumbnailVOList.Count; i++)
        {
			gameObj = (GameObject)Instantiate(prefab);
			gameObj.transform.SetParent(container, false);
            _thumbnailInstantiated = gameObj.GetComponent<Thumbnail>();
            _thumbnailInstantiated.GetComponent<RectTransform>().sizeDelta = _cellSize;
            _thumbnailInstantiated.thumbnailVO = _thumbnailVOList[i];
            _thumbnailInstantiated._thumbnailPoolingCount = _prefabPooling;
            _thumbnailInstantiated._totalThumbnailCount = _maxPrefabLimit;
            _thumbnailInstantiated._constraintCount = _constraintCount;
            positionThumbnail(i, gameObj);
        }
	}

    /// <summary>
    /// Position the thumbnails in the grid format without using the restrictive Grid Layout Group component.
    /// </summary>
    /// <param name="index">Thumbnail index</param>
    /// <param name="thumbnail">Thumbnail object</param>
    private void positionThumbnail(int index, GameObject thumbnail)
    {
        _xPos = _xBuffer + (_totalXMovement * (index % _constraintCount));
        _yPos = _yBuffer + (_totalYMovement * Mathf.FloorToInt(index / _constraintCount));

        thumbnail.transform.localPosition = new Vector3(_xPos, -_yPos, 0f);
    }
}
